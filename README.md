### APF 支付宝当面付框架

- @Author Dioxide.CN
- @Version 2.1.3
- @License GPL-3.0

开发者文档：https://dioxide-cn.ink/?p=81

使用者文档：https://dioxide-cn.ink/?p=85

### 声明

需要在 /lib/v1/phpdelivr/alipayApi/config.php 中配置当面付信息

不得套壳变更参数后重新发布为自己的项目！