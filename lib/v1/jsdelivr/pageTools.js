/*** 全局变量区 ***/
var paidAmount = 1;
var flag = false
/*** 方法区 ***/
function hello(){
    console.log("hello")
}
var vue = new Vue({
	el: "#alipay",
	data: function() {
		return {
			//创建对象
			floatInfo: {
			    alipay: "https://s4.ax1x.com/2022/01/22/7fbmFO.jpg",
			    directory: "https://cdn.jsdelivr.net/gh/vtrois/kratos@4.0.2"
			}
		};
	},
	methods: {
	    //弹出层
		demo() {
			layer.open({
            type: 1,
            //宽 * 高
            area: ['520px', '420px'],
            //标题
            title: "打赏",
            resize: false,
            //滚动条
            scrollbar: true,
            content:
                `<style type="text/css">
                	.box-title-on{
                	    border-radius: 5px;
                	    margin: 5px 15px;
                	    border: 1px solid rgba(0,0,0,.3);
                	}
                	.input-group-dio{
                	    display: flex;
                	}
                	.input-group-dio-addon{
                	    text-align: center;
                	    color: rgba(0,0,0,.5);
                	    padding: 4px 6px;
                	    border: none;
                	}
                	.form-control-dio{
                		outline:none;
                		width: 80%;
                	    background: transparent;
                	    color: rgba(0,0,0,.3);
                	    border: none;
                	    border-left: 1px solid rgba(0,0,0,.3);
                	}
                	.form-control-dio:-ms-input-placeholder {
                	    color: rgba(0,0,0,.3);
                	}
                	.form-control-dio::-webkit-input-placeholder {
                	    color: rgba(0,0,0,.3);
                	}
                	.form-control-dio:-moz-placeholder {
                	    color: rgba(0,0,0,.3);
                	}
                	.form-control-dio:focus{
                		outline:none;
                	    border: none;
                	    border-left: 1px solid rgba(0,0,0,.3);
                	}
                	.check_view_state + .cbs-lay {
                	    cursor: pointer;
                	    font-size: 1em;
                	    padding: 1px!important;
                	    width: 20px;
                	    height: 20px;
                	}
                	[id^="cbs-"] + .cbs-lay {
                	    background-color: #ffffff;
                	    border: 1px solid rgba(0,0,0,.3);
                	    color: #fff;
                	    padding: 9px;
                	    border-radius: 3px;
                	    display: inline-block;
                	    vertical-align: middle;
                	    transition: 0.2s all ease-in-out;
                	}
                	[id="cbs-1"]:checked + .cbs-lay {
                	    color: rgba(0,0,0,.3);
                	    border: 1px solid rgba(0,0,0,.3);
                	}
                	.cbs-lay font{
                		position: relative;
                		font-size: 90%;
                		top: -5px;
                		left: 2px;
                	}
                	.status {
                		position: relative;
                		color: rgba(0,0,0,.3);
                		left: 1px;
                		top: -2px;
                	}
                </style>
                <div id="floatBox" class="donate-box" style="display:flex;padding:10px;position:relative;">
                    <div id="qrCode" class="qr-pay text-center" style="margin-right:10px;width:40%;position:relative;">
                        <img class="pay-img" style="filter: blur(4px);" id="alipay_qr" src="` + this.floatInfo.alipay + `">
                        <div style="position:absolute;top:40%;left:32%;color:#fff;line-height:20px;"><i class="fas fa-sync"></i><br>Biu一下嘛</div>
                    </div>
                    <div style="display: block;width: 100%;">
                        <div class="choose-pay text-center mt-2" style="color:rgba(0,0,0,.6);margin-bottom:5px;text-align:left!important;padding-left:5%;">
                            <img style="width:22%;margin-right:2%;" src="` + this.floatInfo.directory + `/assets/img/payment/alipay.png">
                            使用支付宝当面付扫码
                        </div>
                        <!--分栏-->
                        <div id="msgBox" style="width: 90%;border-radius: 5px;border: 1px solid #eee;display: flex;font-size:90%;">
                        	<a href="javascript:reNum(1);" id="tarNum1" style="border-right: 1px solid #eee;border-radius: 5px 0 0 5px;width: 25%;text-align: center;padding: 4px 8px;color:rgba(0,0,0,.7);box-shadow: inset 2px 2px 4px rgba(0,0,0,.5);">
                        	    1 <font style="font-size:80%;color:rgba(0,0,0,.6)">RMB</font>
                        	</a>
                        	<a href="javascript:reNum(3);" id="tarNum2" style="border-right: 1px solid #eee;width: 25%;text-align: center;padding: 4px 8px;color:rgba(0,0,0,.7);box-shadow: none;">
                        	    3 <font style="font-size:80%;color:rgba(0,0,0,.6)">RMB</font>
                        	</a>
                        	<a href="javascript:reNum(5);" id="tarNum3" style="border-right: 1px solid #eee;width: 25%;text-align: center;padding: 4px 8px;color:rgba(0,0,0,.7);box-shadow: none;">
                        	    5 <font style="font-size:80%;color:rgba(0,0,0,.6)">RMB</font>
                        	</a>
                        	<a href="javascript:reNum(10);" id="tarNum4" style="border-radius: 0 5px 5px 0;width: 25%;text-align: center;padding: 4px 8px;color:rgba(0,0,0,.7);box-shadow: none;">
                        	    10 <font style="font-size:80%;color:rgba(0,0,0,.6)">RMB</font>
                        	</a>
                        </div>
                        <div id="footerBtn" style="position:absolute;bottom:12px;left:79%;">
                            <a id="confIcon" href="javascript:sendArgs();" style="border-radius: 5px;border: 1px solid rgba(60,179,113,.8);padding: 6px 13px;color:rgba(60,179,113,.6);">
                                Biu <i class="fas fa-rocket"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div style="width:100%;display:flex;">
                    <div style="width: 60%;">
                    	<div class="box-title-on">
                        	<div class="input-group-dio">
                        		<div class="input-group-dio-addon">名称</div>
                        		<input id="inName" type="text" class="form-control-dio" placeholder="如何称呼您..." name="name" maxlength="32">
                        	</div>
                        </div>
                        <div class="box-title-on">
                        	<div class="input-group-dio">
                        		<div class="input-group-dio-addon">邮箱</div>
                        		<input id="inMail" type="text" class="form-control-dio" placeholder="邮箱地址..." name="email" maxlength="32">
                        	</div>
                        </div>
                        <div class="box-title-on">
                        	<div class="input-group-dio">
                        		<div class="input-group-dio-addon">备注</div>
                        		<input id="inText" type="text" class="form-control-dio" placeholder="联系方式等任何备注..." name="text" maxlength="32">
                        	</div>
                        </div>
                    </div>
                    <div style="width:40%;display:block;margin: 10px 10px 5px 10px">
                        <input onchange="oBtAddChoice();" type="checkbox" value="0" class="check_view_state"
        id="cbs-1" style="display: none;">
                        <label class="cbs-lay" for="cbs-1"><font>✔</font></label>
                        <span class="status">匿名打赏</span>
                        <div style="font-size:80%;color:rgba(0,0,0,.3);line-height:16px;">
                            说明：您的打赏信息会被公布在“鸣谢单”内，这些资金将用于慈善事业、博客或项目维护上。所有匿名打赏将统一记录在匿名用户上，感谢您对我的博客的大力支持。
                        </div>
                    </div>
                </div>`
            })
		}
	}
})
function reNum(num) {
    if (num == 1) {
        $('#tarNum1').css('box-shadow','inset 2px 2px 4px rgba(0,0,0,.5)');
        $('#tarNum2').css('box-shadow','none');
        $('#tarNum3').css('box-shadow','none');
        $('#tarNum4').css('box-shadow','none');
        this.paidAmount = num;
    } else if (num == 3) {
        $('#tarNum1').css('box-shadow','none');
        $('#tarNum2').css('box-shadow','inset 2px 2px 4px rgba(0,0,0,.5)');
        $('#tarNum3').css('box-shadow','none');
        $('#tarNum4').css('box-shadow','none');
        this.paidAmount = num;
    } else if (num == 5) {
        $('#tarNum1').css('box-shadow','none');
        $('#tarNum3').css('box-shadow','inset 2px 2px 4px rgba(0,0,0,.5)');
        $('#tarNum2').css('box-shadow','none');
        $('#tarNum4').css('box-shadow','none');
        this.paidAmount = num;
    } else if (num == 10) {
        $('#tarNum1').css('box-shadow','none');
        $('#tarNum2').css('box-shadow','none');
        $('#tarNum3').css('box-shadow','none');
        $('#tarNum4').css('box-shadow','inset 2px 2px 4px rgba(0,0,0,.5)');
        this.paidAmount = num;
    }
}
function sendArgs() {
    var inName = ""
    var inMail = ""
    var inText = ""
    if(!this.flag){
        if($("#inName").val() == "" || $("#inMail").val() == "" || $("#inText").val() == "") {
            layer.open({
                type: 1,
                //宽 * 高
                area: ['260px', '80px'],
                //标题
                title: "出错了！",
                resize: false,
                //滚动条
                scrollbar: true,
                content: '<div style="width:100%;text-align:center;color:rgba(220,20,60,.7);">填写信息不完整</div>'
            })
            return false
        }
		//公开信息
		inName = $("#inName").val()
		inMail = $("#inMail").val()
		inText = $("#inText").val()
	} else {
	    inName = "匿名"
		inMail = "隐藏"
		inText = "匿名发送"
	}
    
    var qrCode = document.getElementById("qrCode");
    var crIcon = document.getElementById("confIcon");
    var msgBox = document.getElementById("msgBox");
    var paidM = this.paidAmount;
    /*** jq请求接口类 ***/
	$.ajax({
		type: "POST",
		url: "https://demo.demo/lib/v1/phpdelivr/alipayApi/query.bill.php",
		dataType: "json",
		data: {
			"action": "pay",
			"title": "打赏",
			"money": paidM,
			"notes": "默认备注"
		},
		success: function(res) {
		    //状态码：0:超时 1:待扫码 2:待付款 3:完成
		    var statue = 1;
		    qrCode.innerHTML = '<img class="pay-img" id="alipay_qr" src=https://api.pwmqr.com/qrcode/create/?url='
		        + res.data.qr_code
		        + '>';
		    $('#inName').attr("disabled","disabled");
        	$('#inMail').attr("disabled","disabled");
        	$('#inText').attr("disabled","disabled");
        	$('.check_view_state').attr("disabled","disabled");
		    $('#footerBtn').css('left','74%');
            $('#confIcon').removeAttr('href');
	        $('#confIcon').css('color','rgba(106,90,205,.6)');
	        $('#confIcon').css('border','1px solid rgba(106,90,205,.8)');
	        crIcon.innerHTML = '待扫码 <i class="fa fa-coffee"></i>';
	        msgBox.innerHTML = `
	            <p style="font-size:70%;color:rgba(0,0,0,.65);margin:8px;">请确认您的订单：<br>金额：`
	            + paidM
	            + ` RMB<br>订单号：`
	            + res.data.out_trade_no
	            + `</p>`;
		    //创建间隔5s的计时器判定订单状态
		    var sh = setInterval(function() {
            	$.ajax({
            	    type: "GET",
            	    url: "https://demo.demo/lib/v1/phpdelivr/alipayApi/check.bill.php",
            	    dataType: "json",
            		data: {
            			"action": "query",
            			"outTradeNo": res.data.out_trade_no
            		},
            		success: function(log) {
            		    //检测状态从内存池动态加载信息避免每次都重载innerHTML
                		if (log.code == 200) { //支付完成
                		    statue = 3;
                		    $('#inName').attr("disabled","disabled");
                        	$('#inMail').attr("disabled","disabled");
                        	$('#inText').attr("disabled","disabled");
                		    $('#footerBtn').css('left','68%');
                		    crIcon.innerHTML = '信号已到达 <i class="fa fa-check"></i>';
                		    qrCode.innerHTML = '<img class="pay-img" id="alipay_qr" style="border-radius:5px;width:200px;height:200px;" src="https://s4.ax1x.com/2022/01/22/7hGyN9.jpg">';
                		    msgBox.innerHTML = '';
                		    $('#confIcon').removeAttr('href');
            		        $('#confIcon').css('color','rgba(255,215,0,.6)');
            		        $('#confIcon').css('border','1px solid rgba(255,215,0,.8)');
            		        msgBox.innerHTML = `
            		            <p style="font-size:70%;color:rgba(0,0,0,.65);margin:8px;">信号已收到！<br>感谢您投喂的 `
            		            + paidM
            		            + ` RMB ～`;
            		        $.ajax({
                                type: "GET",
                                url: "https://demo.demo/lib/v1/phpdelivr/jsonBase/base.write.payHelpers.php",
                            	data: {
                            		"name": inName,
                            		"mail": inMail,
                            		"text": inText,
                            		"singleAmount": paidM
                            	},
                            	success: function(logOn) {
                            		console.log(logOn)
                            	},
                            	error: function(errOn) {
                            		console.log(errOn)
                            	}
                            });
            		        //弹出计时器
                		    clearInterval(sh);
                		} else if (log.code == 202) { //未完成
                		    if (log.msg == "未付款交易超时关闭" && statue != 0) {
                		        statue = 0;
                    		    $('#inName').attr("disabled","disabled");
                            	$('#inMail').attr("disabled","disabled");
                            	$('#inText').attr("disabled","disabled");
                		        $('#confIcon').css('color','rgba(220,20,60,.6)');
                		        $('#confIcon').css('border','1px solid rgba(220,20,60,.8)');
                		        $('#confIcon').removeAttr('href');
                		        crIcon.innerHTML = '超时啦 <i class="fa fa-times"></i>';
                		        qrCode.innerHTML = '<img class="pay-img" id="alipay_qr" style="border-radius:5px;width:200px;height:200px;" src="https://s4.ax1x.com/2022/01/04/TqsfyQ.jpg">';
                		        msgBox.innerHTML = '';
                		        msgBox.innerHTML = `
                		            <p style="font-size:70%;color:rgba(0,0,0,.65);margin:8px;">订单似乎超时了呢<br>每个订单只能存活5分钟哦～</p>`;
                		        //弹出计时器
                		        clearInterval(sh);
                		    } else if (log.msg == "等待买家付款" && statue != 2) {
                		        statue = 2
                		        $('#confIcon').css('color','rgba(30,144,255,.6)');
                		        $('#confIcon').css('border','1px solid rgba(30,144,255,.8)');
                		        crIcon.innerHTML = '待付款 <i class="fa fa-coffee"></i>';
                		    }
                		}
            		},
            		error: function (err) {
            		    console.log(err);
            		    clearInterval(sh);
            		}
            	});
            }, 5000);
		},
		error: function (data) {
		    console.log(data);
		}
	});
}
//是否匿名提交
function oBtAddChoice(){
	var is_addChoice = $(".check_view_state").is(':checked')
    if (is_addChoice == true){
    	//匿名
    	this.flag = true
    	$('#inName').attr("disabled","disabled");
    	$('#inMail').attr("disabled","disabled");
    	$('#inText').attr("disabled","disabled");
    } else {
    	//公开
    	this.flag = false
    	$('#inName').removeAttr("disabled");
    	$('#inMail').removeAttr("disabled");
    	$('#inText').removeAttr("disabled");
    }
}