<?php
    //跨域配置，测试时开启
    //header('Access-Control-Allow-Origin: *');
    $url = dirname(dirname(dirname(dirname(__FILE__)))).'/jsonbase/payHelpers.json';
    //处理数据池
    $name = $_GET['name'];
    $mail = $_GET['mail'];
    $text = $_GET['text'];
    $single = $_GET['singleAmount'];
    //获取JSON
    $get_json = file_get_contents($url);
    $arr = json_decode($get_json, true);
    
    $flag = false;
    //遍历查询是否存在相同name
    foreach ($arr['providers'] as $index=>$obj) {
        //存在相同
        if ($obj['name'] == $name || $obj['mail'] == $mail) {
            //覆盖数据流
            $arr['providers'][$index]['name'] = $name; //名称
            $arr['providers'][$index]['mail'] = $mail; //邮箱
            $arr['providers'][$index]['text'] = $text; //备注
            $arr['providers'][$index]['amount'] = $arr['providers'][$index]['amount'] + $single; //累计金额
            $arr['providers'][$index]['recent'] = time(); //最近一次时间
            //确认存在
            $flag = true;
            break;
        } else {
            //不存在创建新数据流
            $data = array(
                "name" => $name,
                "mail" => $mail,
                "text" => $text,
                "amount" => $single,
                "recent" => time()
            );
        }
    }
    
    if (!$flag) {
        //未存在插入数据
        array_push($arr['providers'],$data);
    }
    
    //写入文件
    file_put_contents($url, json_encode($arr));
?>