<?php
    //跨域配置，测试时开启
    //header('Access-Control-Allow-Origin: *');
    $url = dirname(dirname(dirname(dirname(__FILE__)))).'/jsonbase/payHelpers.json';
    //获取JSON
    $get_json = file_get_contents($url);
    $arr = json_decode($get_json, true);
    
    foreach ($arr['providers'] as $index=>$obj) {
        $arr['providers'][$index]['amount'] = $obj['amount'] . " RMB";
        $arr['providers'][$index]['recent'] = date("Y-m-d H:i",$obj['recent']);
        if ($obj['name'] != "匿名") {
            $wait = explode("@",$obj['mail']);
            $arr['providers'][$index]['mail'] = substr($wait[0],0,3) . "******@" . $wait[1];
        }
    }
    
    echo(json_encode($arr));
?>