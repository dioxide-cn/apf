<?php
	/*** 拦截器 ***/
	!empty($_REQUEST['action']) ? $action = $_REQUEST['action'] : exit(json_encode(['code'=>202, "msg"=>"args null"],JSON_UNESCAPED_UNICODE));
	/*** 老规矩 ***/
	include_once 'query.class.php';
	include_once 'config.php';
	$outTradeNo = $_REQUEST['outTradeNo'];
	$aliPay = new AlipayQuery();
	$aliPay->setAppid($pay_config['appid']);
	$aliPay->setRsaPrivateKey($pay_config['private_key']);
	$aliPay->setOutTradeNo($outTradeNo);
	$result = $aliPay->doQuery();
	//老模式：获取到code返回值
	if($result['alipay_trade_query_response']['code'] != '10000') {
		$value = array('code'=>202,'msg'=>$result['alipay_trade_query_response']['sub_msg']);
	} else {
		switch($result['alipay_trade_query_response']['trade_status']){
			case 'WAIT_BUYER_PAY': $value = array('code'=>202,'msg'=>'等待买家付款'); break;
			case 'TRADE_CLOSED': $value = array('code'=>202,'msg'=>'未付款交易超时关闭');break;
			case 'TRADE_SUCCESS': $value = array('code'=>200,'msg'=>'支付成功'); break; 
			case 'TRADE_FINISHED': $value = array('code'=>202,'msg'=>'交易结束'); break;
			default: $value = array('code'=>202,'msg'=>'未知状态'); break;
		}
	}
	//若请求类型为query
	if($action == "query") {
		//则将$value打包为json信息返回
		echo json_encode($value,JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);
	} else if($action == "out") { 
		if(!empty($value['code']== 200)):
		endif; 
	} else { 
		echo 'error';
	}
?>