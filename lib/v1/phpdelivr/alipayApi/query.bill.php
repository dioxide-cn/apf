<?php
    /*** 接口参数 ***/
	$payAmount = $_POST['money'];                             //订单金额
	$orderName = $_POST['title'];                             //订单标题
	$orderNote = $_POST['notes'];                             //订单备注
	/** 请求类库 ****/
	include_once 'service.class.php';
	include_once 'config.php';
	/*** 配置信息 ***/
	$Orderid = uniqid();			                          //订单号
	/*** 配置结束 ***/
	$aliPay = new AlipayService();                            //实例化当面付对象
	$aliPay->setAppid($pay_config['appid']);                  //调用方法传入  appId
	$aliPay->setNotifyUrl($pay_config['notify']);             //调用方法传入  回调地址 * 需要初始化
	$aliPay->setRsaPrivateKey($pay_config['private_key']);    //调用方法传入  私钥
	$aliPay->setTotalFee($payAmount);                         //调用方法传入  数量
	$aliPay->setOutTradeNo($Orderid);                         //调用方法传入  订单号
	$aliPay->setOrderName($orderName);                        //调用方法传入  订单标题
	$result = $aliPay->doPay();                               //调用方法定义  发出订单请求
	$result = $result['alipay_trade_precreate_response'];     //获得订单请求  有效的返回值
	/*** 返回对象 ***/
	if($payAmount == null || $orderName == null || $orderNote == null) {
	    //拦截器
	    $error = array(
	        "code" => 401,
	        "msg" => "args null"
	    );
	    echo(json_encode($error));
	} else if($result['code'] && $result['code']=='10000') {
	    //成功返回
	    $success = array(
    	    "code" => 200, 
	        "msg" => "success",
    	    "orderId" => $Orderid,
    	    "orderName" => $orderName,
    	    "payAmount" => $payAmount,
    	    "orderNote" => $orderNote,
    	    "data" => $result
    	);
	    echo(json_encode($success));
	} else {
	    //异常抛出
	    $fail = array(
    	    "code" => 0, 
	        "msg" => "fail",
    	    "orderId" => $Orderid,
    	    "orderName" => $orderName,
    	    "payAmount" => $payAmount,
    	    "orderNote" => $orderNote,
    	    "data" => $result
    	);
	    echo(json_encode($fail));
	}
?>